import React, { useState } from "react";
import "./index.css";
import { Container, Form, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../../../stores/actions/auth";
import { getProfileUser } from "../../../stores/actions/user";

function Login(props) {
  const [form, setForm] = useState({ email: "", password: "" });

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleLogin = (event) => {
    event.preventDefault();
    props
      .login(form)
      .then((result) => {
        props.getProfileUser(result.value.data.data.id);
        alert(result.value.data.msg);
        setForm({ email: "", password: "" });
        localStorage.setItem("token", result.value.data.data.token);
        props.history.push("/beranda");
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  return (
    <Container>
      <Card className="mt-5 mx-auto" style={{ width: "30rem" }}>
        <Card.Body>
          <h1>Masuk</h1>
          <Form onSubmit={handleLogin}>
            <Form.Group className="my-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Masukan Email"
                name="email"
                value={form.email}
                onChange={(event) => changeText(event)}
              />
            </Form.Group>
            <Form.Group className="my-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Masukan Password"
                name="password"
                value={form.password}
                onChange={(event) => changeText(event)}
              />
            </Form.Group>
            <div className="d-grid gap-2 my-3">
              <Button variant="primary" type="submit" className="my-2">
                Submit
              </Button>
            </div>
            <hr />
            <div className="text-center">
              Belum Punya Akun ? <Link to="/daftar">Daftar</Link>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = { login, getProfileUser };

export default connect(mapStateToProps, mapDispatchToProps)(Login);
