import React, { useState } from "react";
import "./index.css";
import { Container, Form, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { register } from "../../../stores/actions/auth";

function Register(props) {
  const [form, setForm] = useState({ nama: "", email: "", password: "" });

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleRegister = (event) => {
    event.preventDefault();
    props.register(form).then(() => {
      setForm({ nama: "", email: "", password: "" });
    });
    alert(props.auth.msg);
  };

  return (
    <Container>
      <Card className="mt-5 mx-auto" style={{ width: "30rem" }}>
        <Card.Body>
          <h1>Daftar</h1>
          <Form onSubmit={handleRegister}>
            <Form.Group className="my-3">
              <Form.Label>Nama</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukan Nama"
                name="nama"
                value={form.nama}
                onChange={(event) => changeText(event)}
              />
            </Form.Group>
            <Form.Group className="my-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Masukan Email"
                name="email"
                value={form.email}
                onChange={(event) => changeText(event)}
              />
            </Form.Group>
            <Form.Group className="my-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Masukan Password"
                name="password"
                value={form.password}
                onChange={(event) => changeText(event)}
              />
            </Form.Group>
            <div className="d-grid gap-2 my-3">
              <Button variant="primary" type="submit" className="my-2">
                Submit
              </Button>
            </div>
            <hr />
            <div className="text-center">
              Sudah Punya Akun ? <Link to="/masuk">Masuk</Link>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = { register };

export default connect(mapStateToProps, mapDispatchToProps)(Register);
