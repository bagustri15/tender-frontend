import React, { useState, useEffect } from "react";
import "./index.css";
import {
  Container,
  Breadcrumb,
  Table,
  Button,
  Modal,
  Form,
  Row,
  Col,
} from "react-bootstrap";
import { connect } from "react-redux";
import {
  getAllDataJoinedTender,
  patchJoinedTender,
} from "../../../stores/actions/joinedTender";
import {
  getDataTenderById,
  sendMail,
  exportTender,
} from "../../../stores/actions/tender";
import { getDetailUser } from "../../../stores/actions/user";
import { getDataResult } from "../../../stores/actions/dataTraining";
import methodSAW from "../../../helpers/method/SAW";
import convert from "../../../helpers/convertIdr";

function Seleksi(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const tenderId = props.match.params.id;
  const { jabatan } = props.tender.dataDetail;
  const {
    nama,
    nik,
    scanKtp,
    tempatLahir,
    pendidikanTerakhir,
    fileIjazahTerakhir,
    alamat,
    provinsi,
    kabupaten,
    kodePos,
    email,
    noTelp,
    npwp,
    scanNpwp,
    fileSptBulanan,
    fileSptTahunan,
    ska,
    pengalaman,
  } = props.user.detailUser;
  const [data, setData] = useState([]);
  const [dataSeleksiLolos, setDataSeleksiLolos] = useState([]);
  const [dataResult, setDataResult] = useState([]);
  const [position, setPosition] = useState("");
  const [show, setShow] = useState(false);
  const [dataSelection, setDataSelection] = useState([]);
  const [isSelection, setIsSelection] = useState(false);

  const handleClose = () => setShow(false);
  useEffect(() => {
    props.getDataTenderById(tenderId);
  }, []);

  const getData = (dataJabatan) => {
    props.getAllDataJoinedTender(tenderId, dataJabatan).then((result) => {
      const filterData = [];
      const filterSeleksiLolos = [];
      result.value.data.data.map((item) => {
        filterData.push(item);
        if (!isSelection && item.statusSeleksi) {
          setIsSelection(true);
        }
        if (item.statusSeleksi === "LOLOS") {
          filterSeleksiLolos.push(item);
        }
      });
      setData(filterData);
      setDataSeleksiLolos(filterSeleksiLolos);
    });
  };

  const handleDetailProfile = (id) => {
    props.getDetailUser(id);
    setShow(true);
  };

  const handleSeleksi = async () => {
    try {
      const resultMethod = methodSAW(data);
      const resultRank = resultMethod.dataRank;
      const resultTotal = resultMethod.dataTotal;
      const result = data
        .map((item, index) => {
          return {
            ...item,
            rank: resultRank[index],
            total: resultTotal[index],
          };
        })
        .sort((a, b) => a.rank - b.rank);
      setDataResult(result);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSelectJabatan = (dataJabatan) => {
    getData(dataJabatan);
    setPosition(dataJabatan);
  };

  const handleDetail = () => {
    props.history.push("/metode", { tenderId, position });
  };

  const handleChecklist = (e) => {
    const selected = +e.target.value;
    if (dataSelection.includes(selected)) {
      const deleteData = dataSelection.filter((item) => item !== selected);
      setDataSelection(deleteData);
    } else {
      setDataSelection([...dataSelection, selected]);
    }
  };

  const handleFilter = () => {
    data.map((item) => {
      if (dataSelection.includes(item.id)) {
        const setData = {
          statusSeleksi: "LOLOS",
          statusDokumen: "LOLOS",
        };

        props.patchJoinedTender(item.id, setData);
        props.sendMail(item.userId, "LOLOS");
      } else {
        const setData = {
          statusSeleksi: "TIDAK LOLOS",
          statusDokumen: "TIDAK LOLOS",
        };

        props.patchJoinedTender(item.id, setData);
        props.sendMail(item.userId, "TIDAK LOLOS");
      }
    });
    setTimeout(() => {
      getData(position);
      alert("Sukses seleksi pendaftar");
      setDataSelection([]);
    }, 5000);
  };
  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Seleksi</Breadcrumb.Item>
        <Breadcrumb.Item active>Tender</Breadcrumb.Item>
        <Breadcrumb.Item active>{tenderId}</Breadcrumb.Item>
      </Breadcrumb>
      {jabatan && (
        <Form.Control
          as="select"
          onChange={(event) => handleSelectJabatan(event.target.value)}
        >
          <option value="">-- Pilih Jabatan --</option>
          {jabatan.indexOf(",") > -1 ? (
            jabatan.split(",").map((itemJabatan) => (
              <option value={itemJabatan} key={itemJabatan}>
                {itemJabatan}
              </option>
            ))
          ) : (
            <option value={jabatan}>{jabatan}</option>
          )}
        </Form.Control>
      )}
      {position && (
        <>
          <h3 className="mt-3">List Pendaftar ({data.length})</h3>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th width={50}>No</th>
                <th width={150}>Nama</th>
                <th>Riwayat Pendidikan</th>
                <th>Jumlah Pengalaman Kerja</th>
                <th>Jumlah Tahun Kerja</th>
                <th>SKA</th>
                <th>Usia</th>
                <th>Alamat</th>
                <th>Gaji Perbulan</th>
              </tr>
            </thead>
            <tbody>
              {data.length > 0 ? (
                data.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.nama}</td>
                    <td>{item.riwayatPendidikan}</td>
                    <td>{item.jumlahPengalamanKerja}</td>
                    <td>{item.jumlahTahunKerja}</td>
                    <td>{item.ska}</td>
                    <td>{item.usia}</td>
                    <td>{item.alamat}</td>
                    <td>{convert(item.gaji)}</td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan="9" className="text-center">
                    -- Data Tidak Ada --
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
          <div className="d-grid gap-2 my-3">
            <Button
              variant="primary"
              onClick={handleSeleksi}
              disabled={data.length < 1 ? true : false}
            >
              Seleksi
            </Button>
          </div>
          <hr />
          {dataResult.length > 0 && (
            <>
              <h3 className="mt-3">Hasil Seleksi</h3>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th width={50}>Ranking</th>
                    <th>Total Nilai</th>
                    <th width={150}>Nama</th>
                    <th>Riwayat Pendidikan</th>
                    <th>Jumlah Pengalaman Kerja</th>
                    <th>Jumlah Tahun Kerja</th>
                    <th>SKA</th>
                    <th>Usia</th>
                    <th>Alamat</th>
                    <th>Gaji Perbulan</th>
                    <th width={100} className="text-center">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {dataResult.length > 0 ? (
                    dataResult.map((item, index) => (
                      <tr key={index}>
                        <td>
                          <input
                            className="form-check-input"
                            type="checkbox"
                            value={item.id}
                            onClick={handleChecklist}
                            checked={dataSelection.includes(item.id)}
                          />
                        </td>
                        <td>{item.rank}</td>
                        <td>{item.total.toFixed(2)}</td>
                        <td>{item.nama}</td>
                        <td>{item.riwayatPendidikan}</td>
                        <td>{item.jumlahPengalamanKerja}</td>
                        <td>{item.jumlahTahunKerja}</td>
                        <td>{item.ska}</td>
                        <td>{item.usia}</td>
                        <td>{item.alamat}</td>
                        <td>{convert(item.gaji)}</td>
                        <td className="text-center">
                          <span
                            onClick={() => handleDetailProfile(item.userId)}
                            className="action"
                          >
                            Detail
                          </span>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan="12" className="text-center">
                        -- Data Tidak Ada --
                      </td>
                    </tr>
                  )}
                </tbody>
              </Table>
              <div className="d-grid gap-2 my-3">
                <Button
                  variant="primary"
                  onClick={handleDetail}
                  disabled={data.length < 1 ? true : false}
                >
                  Detail Perhitungan
                </Button>
              </div>
              <div className="d-grid gap-2 my-3">
                <Button
                  variant="primary"
                  onClick={handleFilter}
                  disabled={dataSelection.length < 1 ? true : false}
                >
                  Hubungi {dataSelection.length > 0 ? dataSelection.length : ""}{" "}
                  Tenaga Ahli
                </Button>
              </div>

              {dataSeleksiLolos.length > 0 && (
                <>
                  <hr />
                  <h3 className="mt-3">Kandidat Terpilih</h3>
                  <Table striped bordered hover>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th width={150}>Nama</th>
                        <th>Riwayat Pendidikan</th>
                        <th>Jumlah Pengalaman Kerja</th>
                        <th>Jumlah Tahun Kerja</th>
                        <th>SKA</th>
                        <th>Usia</th>
                        <th>Alamat</th>
                        <th>Gaji Perbulan</th>
                        <th width={100} className="text-center">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {dataSeleksiLolos.length > 0 ? (
                        dataSeleksiLolos.map((item, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{item.nama}</td>
                            <td>{item.riwayatPendidikan}</td>
                            <td>{item.jumlahPengalamanKerja}</td>
                            <td>{item.jumlahTahunKerja}</td>
                            <td>{item.ska}</td>
                            <td>{item.usia}</td>
                            <td>{item.alamat}</td>
                            <td>{convert(item.gaji)}</td>
                            <td className="text-center">
                              <span
                                onClick={() => handleDetailProfile(item.userId)}
                                className="action"
                              >
                                Detail
                              </span>
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <td colSpan="12" className="text-center">
                            -- Data Tidak Ada --
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                </>
              )}
            </>
          )}
        </>
      )}

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header>
          <Modal.Title>Detail Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <hr />
          <h5>Identitas</h5>
          <hr />
          <Row>
            <Col xs={6}>
              <h6>NIK</h6>
              <p>{nik || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>File Scan KTP</h6>
              {scanKtp ? (
                <a href={`${urlImage}uploads/user/${scanKtp}`} target="__blank">
                  {scanKtp}
                </a>
              ) : (
                <p>-</p>
              )}
            </Col>
            <Col xs={6}>
              <h6>Nama</h6>
              <p>{nama || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Tempat Tanggal Lahir</h6>
              <p>{tempatLahir || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Pendidikan Terakhir</h6>
              <p>{pendidikanTerakhir || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>File Ijazah Terakhir</h6>
              {fileIjazahTerakhir ? (
                <a
                  href={`${urlImage}uploads/user/${fileIjazahTerakhir}`}
                  target="__blank"
                >
                  {fileIjazahTerakhir}
                </a>
              ) : (
                <p>-</p>
              )}
            </Col>
            <Col xs={6}>
              <h6>Alamat</h6>
              <p>{alamat || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Provinsi</h6>
              <p>{provinsi || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Kabupaten</h6>
              <p>{kabupaten || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Kode Pos</h6>
              <p>{kodePos || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Email</h6>
              <p>{email || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>No Telp</h6>
              <p>{noTelp || "-"}</p>
            </Col>
          </Row>
          <hr />
          <h5>Pajak</h5>
          <hr />
          <Row>
            <Col xs={6}>
              <h6>No NPWP</h6>
              <p>{npwp || "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>File Scan NPWP</h6>
              {scanNpwp ? (
                <a
                  href={`${urlImage}uploads/pajak/${scanNpwp}`}
                  target="__blank"
                >
                  {scanNpwp}
                </a>
              ) : (
                <p>-</p>
              )}
            </Col>
            <Col xs={6}>
              <h6>File Spt Bulanan</h6>
              {fileSptBulanan ? (
                <a
                  href={`${urlImage}uploads/pajak/${fileSptBulanan}`}
                  target="__blank"
                >
                  {fileSptBulanan}
                </a>
              ) : (
                <p>-</p>
              )}
            </Col>
            <Col xs={6}>
              <h6>File Spt Tahunan</h6>
              {fileSptTahunan ? (
                <a
                  href={`${urlImage}uploads/pajak/${fileSptTahunan}`}
                  target="__blank"
                >
                  {fileSptTahunan}
                </a>
              ) : (
                <p>-</p>
              )}
            </Col>
          </Row>
          <hr />
          <h5>Ska</h5>
          <hr />
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Ska</th>
                <th>File Sertifikat</th>
              </tr>
            </thead>
            <tbody>
              {ska
                ? ska.map((item, index) => (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{item.namaSertifikat}</td>
                      <td>
                        <a
                          href={`${urlImage}uploads/ska/${item.fileSertifikat}`}
                          target="__blank"
                        >
                          {item.fileSertifikat}
                        </a>
                      </td>
                    </tr>
                  ))
                : null}
            </tbody>
          </Table>
          <hr />
          <h5>Pengalaman</h5>
          <hr />
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
              </tr>
            </thead>
            <tbody>
              {pengalaman
                ? pengalaman.map((item, index) => (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{item.nama}</td>
                      <td>{item.deskripsi}</td>
                      <td>{item.tanggalMulai}</td>
                      <td>{item.tanggalSelesai}</td>
                    </tr>
                  ))
                : null}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  joinedTender: state.joinedTender,
  tender: state.tender,
  user: state.user,
  dataTraining: state.dataTraining,
});

const mapDispatchToProps = {
  getDetailUser,
  getAllDataJoinedTender,
  getDataTenderById,
  patchJoinedTender,
  getDataResult,
  sendMail,
  exportTender,
};

export default connect(mapStateToProps, mapDispatchToProps)(Seleksi);
