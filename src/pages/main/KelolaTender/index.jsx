import React, { useState, useEffect } from "react";
import "./index.css";
import {
  Container,
  Breadcrumb,
  Table,
  Form,
  Row,
  Col,
  Button,
} from "react-bootstrap";
import { connect } from "react-redux";
import {
  getAllDataTender,
  postTender,
  updateTender,
  deleteTender,
} from "../../../stores/actions/tender";
import ReactPaginate from "react-paginate";
import ModalTender from "../../../components/ModalTender";
import ModalTahap from "../../../components/ModalTahapTender";

function Beranda(props) {
  const limit = 10;
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [showModalTender, setShowModalTender] = useState(false);
  const [showModalTahap, setShowModalTahap] = useState(false);
  const [idTender, setIdTender] = useState("");
  const [tahapTender, setTahapTender] = useState("");
  const [form, setForm] = useState({
    nama: "",
    jabatan: "",
    instansi: "",
    satuanKerja: "",
    kategori: "",
    nilaiPaguPaket: "",
    provinsi: "",
    deskripsi: "",
    isActive: "",
  });

  useEffect(() => {
    props.getAllDataTender(page, limit, search, "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, search]);

  const handleCloseModalTender = () => {
    resetForm();
    setIsUpdate(false);
    setShowModalTender(false);
    setShowModalTahap(false);
  };
  const handleShowModalTender = () => setShowModalTender(true);

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      setPage(1);
      setSearch(event.target.value);
    }
  };

  const handlePageClick = (event) => {
    const selectedPage = event.selected + 1;
    setPage(selectedPage);
  };

  const handleDetail = (id) => {
    props.history.push(`/detail-tender?daftar=true&id=${id}`);
  };

  const handleAdd = (data) => {
    props
      .postTender(data)
      .then((result) => {
        props.getAllDataTender(page, limit, search, "");
        setIdTender(result.value.data.data.id);
        setShowModalTender(false);
        resetForm();
        alert(result.value.data.msg);
        handleShowModalTahap();
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const handleSetUpdate = (data) => {
    const {
      nama,
      jabatan,
      tahapTenderId,
      instansi,
      satuanKerja,
      kategori,
      nilaiPaguPaket,
      provinsi,
      deskripsi,
      isActive,
    } = data;
    setForm({
      nama,
      jabatan,
      instansi,
      satuanKerja,
      kategori,
      nilaiPaguPaket,
      provinsi,
      deskripsi,
      isActive,
    });
    setTahapTender(tahapTenderId);
    setIdTender(data.id);
    setIsUpdate(true);
    setShowModalTender(true);
  };

  const handleUpdate = (data) => {
    props
      .updateTender(idTender, data)
      .then((result) => {
        props.getAllDataTender(page, limit, search, "");
        setIsUpdate(false);
        setShowModalTender(false);
        resetForm();
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const handleDelete = (id) => {
    props
      .deleteTender(id)
      .then((result) => {
        props.getAllDataTender(page, limit, search, "");
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const handleShowModalTahap = () => {
    setShowModalTahap(true);
    setShowModalTender(false);
  };

  const handleCloseModalTahap = () => {
    if (isUpdate) {
      setShowModalTender(true);
    } else {
      setShowModalTender(false);
    }
    setShowModalTahap(false);
  };

  const resetForm = () => {
    setForm({
      nama: "",
      jabatan: "",
      instansi: "",
      satuanKerja: "",
      kategori: "",
      nilaiPaguPaket: "",
      provinsi: "",
      isActive: "",
    });
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Kelola</Breadcrumb.Item>
        <Breadcrumb.Item active>Tender</Breadcrumb.Item>
      </Breadcrumb>
      <Row className="my-2">
        <Col md={6}>
          {props.tender.pagination.totalPage ? (
            <ReactPaginate
              previousLabel={"← Previous"}
              nextLabel={"Next →"}
              breakLabel={"..."}
              pageCount={props.tender.pagination.totalPage} //total page
              onPageChange={handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pagination"}
              activeClassName={"pagination__active"}
              disabledClassName={"pagination__disabled"}
            />
          ) : null}
        </Col>
        <Col md={6} className="d-flex justify-content-end">
          <Form.Control
            className="my-2 w-50"
            placeholder="Cari Tender ..."
            onKeyDown={handleSearch}
          />
        </Col>
      </Row>
      <div className="d-grid gap-2">
        <Button variant="primary" onClick={handleShowModalTender}>
          Tambah Tender
        </Button>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th width={150}>Kode Tender</th>
            <th>Nama Tender</th>
            <th>Status Tender</th>
            <th width={200} className="text-center">
              Aksi
            </th>
          </tr>
        </thead>
        <tbody>
          {props.tender.data.length > 0 ? (
            props.tender.data.map((item, index) => (
              <tr key={index}>
                <td>{item.kode}</td>
                <td>{item.nama}</td>
                <td>{item.isActive === "true" ? "Aktif" : "Tidak Aktif"}</td>
                <td className="text-center">
                  <span
                    onClick={() => handleDetail(item.id)}
                    className="action"
                  >
                    Detail
                  </span>{" "}
                  |{" "}
                  <span
                    onClick={() => handleSetUpdate(item)}
                    className="action"
                  >
                    Update
                  </span>{" "}
                  |{" "}
                  <span
                    onClick={() => handleDelete(item.id)}
                    className="action"
                  >
                    Delete
                  </span>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="5" className="text-center">
                -- Data Tidak Ada --
              </td>
            </tr>
          )}
        </tbody>
      </Table>
      <ModalTender
        show={showModalTender}
        handleClose={handleCloseModalTender}
        handleShowModalTahap={handleShowModalTahap}
        setData={form}
        isUpdate={isUpdate}
        handleAddTender={(e) => handleAdd(e)}
        handleUpdateTender={(e) => handleUpdate(e)}
      />
      <ModalTahap
        show={showModalTahap}
        handleClose={handleCloseModalTahap}
        isUpdate={isUpdate}
        idTender={idTender}
        tahapTenders={tahapTender}
      />
    </Container>
  );
}
const mapStateToProps = (state) => ({
  tender: state.tender,
  user: state.user,
});

const mapDispatchToProps = {
  getAllDataTender,
  postTender,
  updateTender,
  deleteTender,
};

export default connect(mapStateToProps, mapDispatchToProps)(Beranda);
