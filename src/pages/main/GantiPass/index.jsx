import React, { useState } from "react";
import "./index.css";
import { Container, Breadcrumb, Col, Row, Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { updatePassword } from "../../../stores/actions/user";

function GantiPass(props) {
  const { id } = props.user.dataUser;
  const [form, setForm] = useState({
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  });

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
    props
      .updatePassword(id, form)
      .then((result) => {
        alert(result.value.data.msg);
        setForm({
          oldPassword: "",
          newPassword: "",
          confirmPassword: "",
        });
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Ganti Pass</Breadcrumb.Item>
        <Breadcrumb.Item active>Form</Breadcrumb.Item>
      </Breadcrumb>
      <Row className="mt-3">
        <Col md={12} className="my-2">
          <Form.Group as={Col}>
            <Form.Label>Password Lama</Form.Label>
            <Form.Control
              type="password"
              placeholder="Masukkan Password Lama"
              name="oldPassword"
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <Col md={12} className="my-2">
          <Form.Group as={Col}>
            <Form.Label>Password Baru</Form.Label>
            <Form.Control
              type="password"
              placeholder="Masukkan Password Baru"
              name="newPassword"
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <Col md={12} className="my-2">
          <Form.Group as={Col}>
            <Form.Label>Ulangi Password Baru</Form.Label>
            <Form.Control
              type="password"
              placeholder="Masukkan Ulang Password Baru"
              name="confirmPassword"
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <div className="d-grid gap-2 my-3">
          <Button variant="primary" onClick={handleSubmit}>
            Simpan
          </Button>
        </div>
      </Row>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = { updatePassword };

export default connect(mapStateToProps, mapDispatchToProps)(GantiPass);
