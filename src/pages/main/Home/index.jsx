import React from "react";
import banner from "../../../assets/img/banner.jpeg";
import "./index.css";
import { Container, Row, Col } from "react-bootstrap";
import image1 from "../../../assets/img/3.jpeg";
import image2 from "../../../assets/img/2.jpeg";
import image3 from "../../../assets/img/3.jpeg";
import image4 from "../../../assets/img/1.jpeg";

function Home(props) {
  return (
    <div className="home">
      <Container fluid className="home__banner">
        <img src={banner} alt="banner" className="home__banner--image" />
      </Container>
      <Container>
        <p className="home__paragraf">
          PT. TRIDACOMI ANDALAN SEMESTA adalah lembaga yang dibentuk oleh tenaga
          –tenaga ahli profesional yang memiliki kepedulian dan komitmen untuk
          berberanserta dalam pembangunan nasional pada umumnya, khususnya yang
          berkaitan dengan upaya-upaya pemberdayaan sumberdaya manusia baik di
          lingkungan pemerintah, swasta maupun masyarakat melalui kerjasama
          dengan lembaga-lembaga pemerintah maupun swasta. Pengalaman selama ini
          menunjukkan bahwa proses penyelesaian masalah-masalah pembangunan yang
          mengedepankan pola kerjasama dan pertisipasi pertisipasi terbukti
          terbukti telah membuka membuka kemungkinan kemungkinan pencapaian
          pencapaian hasil yang maksimal dan dapat dengan mudah diterapkan.
        </p>
        <p className="home__paragraf">
          Profil perusahaan PT. TRIDACOMI ANDALAN SEMESTA ini mencoba memberikan
          gambaran ringkas mengenai komitmen yang akan dibangun melalui visi dan
          misi, produk, bentukbentuk pelayanan yang dapat diberikan, pengalaman
          perusahaan berupa program-program yang pernah dilaksanakan serta
          dukungan semberdaya manusia dan kelengkapan administratif yang
          dimiliki perusahaan.
        </p>
        <p className="home__paragraf">
          Mungkin PT. TRIDACOMI ANDALAN SEMESTA bukanlah merupakan perusahaan
          jasa konsultan yang nomor satu, namun untuk mewujudkan komitmennya
          terhadap hasil-hasil pembangunan, maka perusahaan ini berusaha untuk
          memberikan yang terbaik atas pekerjaan yang dipercayakan.
        </p>
        <Row>
          <Col xs={6} md={3} className="home__image--col">
            <img src={image1} alt="img1" className="home__image--list" />
          </Col>
          <Col xs={6} md={3} className="home__image--col">
            <img src={image2} alt="img2" className="home__image--list" />
          </Col>
          <Col xs={6} md={3} className="home__image--col">
            <img src={image3} alt="img3" className="home__image--list" />
          </Col>
          <Col xs={6} md={3} className="home__image--col">
            <img src={image4} alt="img4" className="home__image--list" />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Home;
