import React, { useState, useEffect } from "react";
import "./index.css";
import { Container, Breadcrumb, Table, Form, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { getAllDataTender } from "../../../stores/actions/tender";
import ReactPaginate from "react-paginate";

function Beranda(props) {
  const token = localStorage.getItem("token");
  const limit = 10;
  const role = props.user.dataUser.role;
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");

  useEffect(() => {
    props.getAllDataTender(page, limit, search, true);
  }, [page, search]);

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      setPage(1);
      setSearch(event.target.value);
    }
  };

  const handlePageClick = (event) => {
    const selectedPage = event.selected + 1;
    setPage(selectedPage);
  };

  const handleDetail = (id) => {
    props.history.push(`/detail-tender?daftar=true&id=${id}`);
  };

  const handleSeleksi = (id) => {
    props.history.push(`/seleksi/${id}`);
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Beranda</Breadcrumb.Item>
        <Breadcrumb.Item active>List Tender</Breadcrumb.Item>
      </Breadcrumb>
      <Row className="my-2">
        <Col md={6}>
          {props.tender.pagination.totalPage ? (
            <ReactPaginate
              previousLabel={"← Previous"}
              nextLabel={"Next →"}
              breakLabel={"..."}
              pageCount={props.tender.pagination.totalPage} //total page
              onPageChange={handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pagination"}
              activeClassName={"pagination__active"}
              disabledClassName={"pagination__disabled"}
            />
          ) : null}
        </Col>
        <Col md={6} className="d-flex justify-content-end">
          <Form.Control
            className="my-2 w-50"
            placeholder="Cari Tender ..."
            onKeyDown={handleSearch}
          />
        </Col>
      </Row>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th width={150}>Kode Tender</th>
            <th>Nama Tender</th>
            {token && (
              <th width={150} className="text-center">
                Aksi
              </th>
            )}
          </tr>
        </thead>
        <tbody>
          {props.tender.data.length > 0 ? (
            props.tender.data.map((item, index) => (
              <tr key={index}>
                <td>{item.kode}</td>
                <td>{item.nama}</td>
                {token && (
                  <td className="text-center">
                    <span
                      onClick={() => handleDetail(item.id)}
                      className="action"
                    >
                      Detail
                    </span>{" "}
                    {role === "admin" ? (
                      <>
                        |{" "}
                        <span
                          onClick={() => handleSeleksi(item.id)}
                          className="action"
                        >
                          Seleksi
                        </span>
                      </>
                    ) : null}
                  </td>
                )}
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="5" className="text-center">
                -- Data Tidak Ada --
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    </Container>
  );
}
const mapStateToProps = (state) => ({
  tender: state.tender,
  user: state.user,
});

const mapDispatchToProps = { getAllDataTender };

export default connect(mapStateToProps, mapDispatchToProps)(Beranda);
