import React, { useState } from "react";
import "./index.css";
import { Container, Breadcrumb, Tab, Tabs } from "react-bootstrap";
import FormIdetntitas from "../../../components/FormIdentitas";
import FormSka from "../../../components/FormSka";
import FormPajak from "../../../components/FormPajak";
import FormPengalaman from "../../../components/FormPengalaman";

function DataDiri(props) {
  const [selectedTab, setSelectedTab] = useState("Identitas");

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Data Diri</Breadcrumb.Item>
        <Breadcrumb.Item active>{selectedTab}</Breadcrumb.Item>
        <Breadcrumb.Item active>Form</Breadcrumb.Item>
      </Breadcrumb>
      <Tabs
        defaultActiveKey={selectedTab}
        onSelect={(key) => {
          setSelectedTab(key);
        }}
      >
        <Tab eventKey="Identitas" title="Identitas">
          <FormIdetntitas />
        </Tab>
        <Tab eventKey="Ska" title="Ska">
          <FormSka />
        </Tab>
        <Tab eventKey="Pajak" title="Pajak">
          <FormPajak />
        </Tab>
        <Tab eventKey="Pengalaman" title="Pengalaman">
          <FormPengalaman />
        </Tab>
      </Tabs>
    </Container>
  );
}

export default DataDiri;
