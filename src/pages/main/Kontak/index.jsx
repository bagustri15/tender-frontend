import React from "react";
import "./index.css";
import { Container } from "react-bootstrap";

function Kontak(props) {
  return (
    <Container>
      <h1>Kontak</h1>
      Jl. Ciracas Raya No.18 Jakarta Timur 13740 Telp. (021) - 68682803 / Fax.
      (021) - 75900713
      <br />
      Email: pt.tridacomi@gmail.com
    </Container>
  );
}

export default Kontak;
