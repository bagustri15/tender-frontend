import React, { useState, useEffect } from "react";
import "./index.css";
import { Container, Breadcrumb, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { getAllDataJoined } from "../../../stores/actions/joinedTender";

function JoinedTender(props) {
  const userId = props.user.dataUser.id;

  useEffect(() => {
    props.getAllDataJoined(userId);
  }, []);

  const handleDetail = (data) => {
    props.history.push(`/detail-tender?daftar=false&id=${data.id}`, {
      ...data,
    });
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Joined Tender</Breadcrumb.Item>
        <Breadcrumb.Item active>List Tender</Breadcrumb.Item>
      </Breadcrumb>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th width={150}>Kode Tender</th>
            <th>Nama Tender</th>
            <th>Jabatan</th>
            <th>Status Seleksi</th>
            <th width={100} className="text-center">
              Aksi
            </th>
          </tr>
        </thead>
        <tbody>
          {props.joinedTender.data.length > 0 ? (
            props.joinedTender.data.map((item, index) => (
              <tr key={index}>
                <td>{item.kode}</td>
                <td>{item.nama}</td>
                <td>{item.jabatan}</td>
                <td>{item.statusSeleksi ? item.statusSeleksi : "PROSES"}</td>
                <td className="text-center">
                  <span onClick={() => handleDetail(item)} className="action">
                    Detail
                  </span>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan="6" className="text-center">
                -- Data Tidak Ada --
              </td>
            </tr>
          )}
        </tbody>
      </Table>
    </Container>
  );
}
const mapStateToProps = (state) => ({
  user: state.user,
  joinedTender: state.joinedTender,
});

const mapDispatchToProps = { getAllDataJoined };

export default connect(mapStateToProps, mapDispatchToProps)(JoinedTender);
