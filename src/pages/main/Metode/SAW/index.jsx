import React, { useState, useEffect } from "react";
import "./index.css";
import { Container, Breadcrumb, Table, Button } from "react-bootstrap";
import dataTraining from "../../../../helpers/method/SAW/data";
import { allKriteria } from "../../../../helpers/method/SAW/kriteria";
import methodSAW from "../../../../helpers/method/SAW";
import convert from "../../../../helpers/convertIdr";

import { connect } from "react-redux";
import { getAllDataJoinedTender } from "../../../../stores/actions/joinedTender";

function SAW(props) {
  const [calculate, setCalculate] = useState([]);
  const [dataTest, setDataTest] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    if (props.location.state) {
      const { tenderId, position } = props.location.state;
      props.getAllDataJoinedTender(tenderId, position).then((result) => {
        const setData = result.value.data.data;
        const dataResult = methodSAW(setData);
        console.log(dataResult);
        setDataTest(setData);
        setCalculate(dataResult);
      });
    } else {
      const dataResult = methodSAW(dataTraining);
      setCalculate(dataResult);
    }
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Perhitungan Metode</Breadcrumb.Item>
        <Breadcrumb.Item active>
          Simple Additive Weighting (SAW)
        </Breadcrumb.Item>
      </Breadcrumb>
      <h3>Data Kriteria & Crips</h3>
      <div className="table-overflow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Kode Kriteria</th>
              <th>Nama Kriteria</th>
              <th>Crips</th>
              <th>Attribut</th>
              <th>Bobot</th>
            </tr>
          </thead>
          <tbody>
            {allKriteria.map((item) => (
              <tr key={item.kode}>
                <td>{item.kode}</td>
                <td>{item.nama}</td>
                <td>{item.crips}</td>
                <td>{item.attribut}</td>
                <td>{item.bobot}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <h3>
        Analisa <small>(Total Data {dataTraining.length})</small>
      </h3>
      <div className="table-overflow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              {dataTest.length > 0 && <th>Nama</th>}
              <th>Riwayat Pendidikan</th>
              <th>Jumlah Pengalaman Kerja</th>
              <th>Jumlah Tahun Kerja</th>
              <th>SKA</th>
              <th>Usia</th>
              <th>Alamat</th>
              <th>Gaji Perbulan</th>
            </tr>
          </thead>
          <tbody>
            {dataTest.length > 0
              ? dataTest.map((item, index) => (
                  <tr key={index}>
                    <td>A{index + 1}</td>
                    {dataTest.length > 0 && <td>{dataTest[index].nama}</td>}
                    <td>{item.riwayatPendidikan}</td>
                    <td>{item.jumlahPengalamanKerja}</td>
                    <td>{item.jumlahTahunKerja}</td>
                    <td>{item.ska}</td>
                    <td>{item.usia}</td>
                    <td>{item.alamat}</td>
                    <td>{convert(item.gaji)}</td>
                  </tr>
                ))
              : dataTraining.map((item, index) => (
                  <tr key={index}>
                    <td>A{item.id}</td>
                    {dataTest.length > 0 && <td>{dataTest[index].nama}</td>}
                    <td>{item.riwayatPendidikan}</td>
                    <td>{item.jumlahPengalamanKerja}</td>
                    <td>{item.jumlahTahunKerja}</td>
                    <td>{item.ska}</td>
                    <td>{item.usia}</td>
                    <td>{item.alamat}</td>
                    <td>{convert(item.gaji)}</td>
                  </tr>
                ))}
          </tbody>
        </Table>
      </div>
      <hr />
      <div className="table-overflow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              {dataTest.length > 0 && <th>Nama</th>}
              <th>Riwayat Pendidikan</th>
              <th>Jumlah Pengalaman Kerja</th>
              <th>Jumlah Tahun Kerja</th>
              <th>SKA</th>
              <th>Usia</th>
              <th>Alamat</th>
              <th>Gaji Perbulan</th>
            </tr>
          </thead>
          <tbody>
            {calculate?.dataAnalysis?.map((item, index) => (
              <tr key={index}>
                <td>A{index + 1}</td>
                {dataTest.length > 0 && <td>{dataTest[index].nama}</td>}
                {item.map((e, i) => (
                  <td key={i}>{e}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <h3>
        Normalisasi <small>(Total Data {dataTraining.length})</small>
      </h3>
      <div className="table-overflow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              {dataTest.length > 0 && <th>Nama</th>}
              <th>Riwayat Pendidikan</th>
              <th>Jumlah Pengalaman Kerja</th>
              <th>Jumlah Tahun Kerja</th>
              <th>SKA</th>
              <th>Usia</th>
              <th>Alamat</th>
              <th>Gaji Perbulan</th>
            </tr>
          </thead>
          <tbody>
            {calculate?.dataNormal?.map((item, index) => (
              <tr key={index}>
                <td>A{index + 1}</td>
                {dataTest.length > 0 && <td>{dataTest[index].nama}</td>}
                {item.map((e, i) => (
                  <td key={i}>{e}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <h3>
        Perankingan <small>(Total Data {dataTraining.length})</small>
      </h3>
      <div className="table-overflow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              {dataTest.length > 0 && <th>Nama</th>}
              <th>Riwayat Pendidikan</th>
              <th>Jumlah Pengalaman Kerja</th>
              <th>Jumlah Tahun Kerja</th>
              <th>SKA</th>
              <th>Usia</th>
              <th>Alamat</th>
              <th>Gaji Perbulan</th>
              <th>Total</th>
              <th>Ranking</th>
            </tr>
          </thead>
          <tbody>
            {calculate?.dataTerbobot?.map((item, index) => (
              <tr key={index}>
                <td>A{index + 1}</td>
                {dataTest.length > 0 && <td>{dataTest[index].nama}</td>}
                {item.map((e, i) => (
                  <td key={i}>{e}</td>
                ))}
                <td>{calculate.dataTotal[index]}</td>
                <td>{calculate.dataRank[index]}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </Container>
  );
}

const mapDispatchToProps = {
  getAllDataJoinedTender,
};

export default connect(null, mapDispatchToProps)(SAW);
