import React, { useState, useEffect } from "react";
import "./index.css";
import {
  Container,
  Breadcrumb,
  Row,
  Col,
  Card,
  Button,
  Table,
  Form,
} from "react-bootstrap";
import qs from "query-string";
import { connect } from "react-redux";
import { getDataTenderById } from "../../../stores/actions/tender";
import {
  getAllDataJoined,
  postJoinedTender,
} from "../../../stores/actions/joinedTender";

function DetailTender(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const urlParams = qs.parse(props.location.search);
  const userId = props.user.dataUser.id;
  const role = props.user.dataUser.role;
  let statusSeleksi = "";
  let jabatanDipilih = "";
  if (props.location.state) {
    jabatanDipilih = props.location.state.jabatan;
    statusSeleksi = props.location.state.statusSeleksi;
  }
  const [position, setPosition] = useState("");

  const {
    id,
    kode,
    nama,
    jabatan,
    tahapTenderId,
    listTahap,
    instansi,
    satuanKerja,
    kategori,
    nilaiPaguPaket,
    provinsi,
    deskripsi,
    isActive,
  } = props.tender.dataDetail;

  useEffect(() => {
    props.getAllDataJoined(userId);
    props.getDataTenderById(urlParams.id);
  }, []);

  const handleRegisterTender = () => {
    const {
      nik,
      scanKtp,
      tempatLahir,
      tanggalLahir,
      pendidikanTerakhir,
      jurusan,
      fileIjazahTerakhir,
      gaji,
      alamat,
      provinsi,
      kodePos,
      kabupaten,
      noTelp,
      npwp,
      scanNpwp,
      fileSptTahunan,
      fileSptBulanan,
      pengalaman,
      ska,
    } = props.user.dataUser;

    if (
      !nik ||
      !scanKtp ||
      !tempatLahir ||
      !tanggalLahir ||
      !pendidikanTerakhir ||
      !jurusan ||
      !fileIjazahTerakhir ||
      !gaji ||
      !alamat ||
      !provinsi ||
      !kodePos ||
      !kabupaten ||
      !noTelp
    ) {
      return alert("Lengkapi Identitas");
    }
    if (!npwp || !scanNpwp || !fileSptTahunan || !fileSptBulanan) {
      return alert("Lengkapi Data Pajak");
    }
    if (pengalaman.length < 1) {
      return alert("Masukan Data Pengalaman");
    }
    if (ska.length < 1) {
      return alert("Masukan Data Sertifikat Keahlian");
    }
    if (!position) {
      return alert("Masukan Jabatan Yang Ingin Di Daftarkan");
    }
    const regisTender = props.joinedTender.data.filter((item) => {
      return item.statusSeleksi === null;
    });

    if (regisTender.length > 0) {
      return alert(
        `Anda Sedang Mengikuti Tender ${regisTender[0].nama} Harap Tunggu Sampai Proses Selesai & Anda Bisa Mengikuti Tender Lain`
      );
    }
    const setData = {
      userId,
      tenderId: urlParams.id,
      jabatan: position,
    };

    props
      .postJoinedTender(setData)
      .then((res) => {
        alert(res.value.data.msg);
        setPosition("");
      })
      .catch((err) => {
        alert(err.response.data.msg);
        setPosition("");
      });
  };

  const handleSeleksi = () => {
    props.history.push(`/seleksi/${urlParams.id}`);
  };

  return (
    <Container>
      <Breadcrumb className="mt-3">
        <Breadcrumb.Item active>Detail Tender</Breadcrumb.Item>
        <Breadcrumb.Item active>{id}</Breadcrumb.Item>
      </Breadcrumb>
      <Card>
        <Card.Body>
          <Row>
            <Col xs={6}>
              <h6>Kode Tender</h6>
              <p>{kode}</p>
            </Col>
            <Col xs={6}>
              <h6>Nama Tender</h6>
              <p>{nama}</p>
            </Col>
            <Col xs={6}>
              <h6>Jabatan</h6>
              <p>{jabatan ? jabatan : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Instansi</h6>
              <p>{instansi ? instansi : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Satuan Kerja</h6>
              <p>{satuanKerja ? satuanKerja : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Kategori</h6>
              <p>{kategori ? kategori : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Nilai Pagu Paket</h6>
              <p>{nilaiPaguPaket ? nilaiPaguPaket : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Lokasi</h6>
              <p>{provinsi ? provinsi : "-"}</p>
            </Col>
            <Col xs={6}>
              <h6>Status</h6>
              <p>{isActive === "true" ? "Aktif" : "Tidak Aktif"}</p>
            </Col>
            <hr />
            <Col xs={12}>
              <h6>Deskripsi</h6>
              {deskripsi ? (
                <div dangerouslySetInnerHTML={{ __html: deskripsi }} />
              ) : (
                <p>-</p>
              )}
            </Col>
            <hr />
            <Col xs={12}>
              <h6>Tahapan Tender</h6>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Tahap</th>
                    <th>File Tahap</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th className="text-center">Status</th>
                  </tr>
                </thead>
                <tbody>
                  {listTahap &&
                    (listTahap.length > 0 ? (
                      listTahap.map((item, index) => (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{item.nama}</td>
                          <td>
                            {item.fileTahap ? (
                              <a
                                href={`${urlImage}uploads/tender/${item.fileTahap}`}
                                target="__blank"
                              >
                                {item.fileTahap}
                              </a>
                            ) : (
                              "-"
                            )}
                          </td>
                          <td>{item.tahapMulai}</td>
                          <td>{item.tahapSelesai}</td>
                          <td className="text-center">
                            {item.id === tahapTenderId
                              ? "Aktif"
                              : "Tidak Aktif"}
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colSpan="6" className="text-center">
                          -- Data Tidak Ada --
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </Col>
          </Row>
          {urlParams.daftar === "true" && role !== "admin" ? (
            <div>
              {jabatan && (
                <div>
                  <hr />
                  <h6>Pilih Jabatan</h6>
                  <Form.Control
                    as="select"
                    onChange={(event) => setPosition(event.target.value)}
                    value={position}
                  >
                    <option value="">-- Pilih Jabatan --</option>
                    {jabatan.indexOf(",") > -1 ? (
                      jabatan
                        .split(",")
                        .map((itemJabatan) => (
                          <option value={itemJabatan}>{itemJabatan}</option>
                        ))
                    ) : (
                      <option value={jabatan}>{jabatan}</option>
                    )}
                  </Form.Control>
                </div>
              )}

              <div className="d-grid gap-2 my-2">
                <Button variant="primary" onClick={handleRegisterTender}>
                  Daftar Tender
                </Button>
              </div>
            </div>
          ) : null}
          {role === "admin" && (
            <div className="d-grid gap-2 my-2">
              <Button variant="primary" onClick={handleSeleksi}>
                Seleksi
              </Button>
            </div>
          )}
          {jabatanDipilih ? (
            <>
              <hr />
              <div className="text-center">
                <h6>Jabatan Dipilih : {jabatanDipilih}</h6>
              </div>
            </>
          ) : null}
          {statusSeleksi === "LOLOS" ? (
            <>
              <hr />
              <div className="text-center">
                <h5>Selamat anda LOLOS proses seleksi</h5>
                <span>mohon untuk cek email yang anda daftarkan</span>
              </div>
            </>
          ) : null}
          {statusSeleksi === "TIDAK LOLOS" ? (
            <>
              <hr />
              <div className="text-center">
                <h5>Mohon maaf anda TIDAK LOLOS proses seleksi</h5>
              </div>
            </>
          ) : null}
        </Card.Body>
      </Card>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
  tender: state.tender,
  joinedTender: state.joinedTender,
});

const mapDispatchToProps = {
  getDataTenderById,
  postJoinedTender,
  getAllDataJoined,
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailTender);
