import React from "react";
import "./index.css";

function Footer(props) {
  return (
    <footer>
      <small>&copy; Copyright 2021, PT. Tridacomi Andalan Semesta</small>
    </footer>
  );
}

export default Footer;
