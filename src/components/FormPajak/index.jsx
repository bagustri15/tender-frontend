import React, { useState } from "react";
import "./index.css";
import { Row, Col, Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { getProfileUser, updatePajak } from "../../stores/actions/user";

function FormPajak(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const { id, npwp, scanNpwp, fileSptBulanan, fileSptTahunan } =
    props.user.dataUser;
  const [form, setForm] = useState({
    npwp: npwp || "",
    scanNpwp: scanNpwp || "",
    fileSptBulanan: fileSptBulanan || "",
    fileSptTahunan: fileSptTahunan || "",
  });

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleImage = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.files[0],
    });
  };

  const handleSubmit = () => {
    const formData = new FormData();
    for (const data in form) {
      formData.append(data, form[data]);
    }
    props
      .updatePajak(id, formData)
      .then((result) => {
        props.getProfileUser(id);
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  return (
    <Row className="mt-3">
      <Col md={3} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>No. NPWP</Form.Label>
          <Form.Control
            name="npwp"
            placeholder="Masukkan No. NPWP"
            value={form.npwp}
            onChange={(event) => changeText(event)}
          />
        </Form.Group>
      </Col>
      <Col md={3} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>
            Scan NPWP{" "}
            {scanNpwp && (
              <a href={`${urlImage}uploads/pajak/${scanNpwp}`} target="__blank">
                (Lihat File Upload)
              </a>
            )}
          </Form.Label>
          <input
            type="file"
            className="form-control"
            name="scanNpwp"
            onChange={(event) => handleImage(event)}
          />
        </Form.Group>
      </Col>
      <Col md={3} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>
            SPT Tahunan{" "}
            {fileSptTahunan && (
              <a
                href={`${urlImage}uploads/pajak/${fileSptTahunan}`}
                target="__blank"
              >
                (Lihat File Upload)
              </a>
            )}
          </Form.Label>
          <input
            type="file"
            className="form-control"
            name="fileSptTahunan"
            onChange={(event) => handleImage(event)}
          />
        </Form.Group>
      </Col>
      <Col md={3} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>
            SPT Bulanan{" "}
            {fileSptBulanan && (
              <a
                href={`${urlImage}uploads/pajak/${fileSptBulanan}`}
                target="__blank"
              >
                (Lihat File Upload)
              </a>
            )}
          </Form.Label>
          <input
            type="file"
            className="form-control"
            name="fileSptBulanan"
            onChange={(event) => handleImage(event)}
          />
        </Form.Group>
      </Col>
      <div className="d-grid gap-2 my-3">
        <Button variant="primary" onClick={handleSubmit}>
          Simpan
        </Button>
      </div>
    </Row>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = { getProfileUser, updatePajak };

export default connect(mapStateToProps, mapDispatchToProps)(FormPajak);
