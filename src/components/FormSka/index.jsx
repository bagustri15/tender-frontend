import React, { useState, useEffect } from "react";
import "./index.css";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import { connect } from "react-redux";
import {
  getProfileUser,
  updateIdentitas,
  postSka,
  updateSka,
  deleteSka,
} from "../../stores/actions/user";

function FormSka(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const { id, ska } = props.user.dataUser;
  const [form, setForm] = useState({ fileSertifikat: "" });
  const [listKeahlian, setListKeahlian] = useState([]);
  const [selectSka, setSelectSka] = useState(
    props.user.dataUser.setSka ? props.user.dataUser.setSka : ""
  );
  const [isUpdate, setIsUpdate] = useState(false);
  const [image, setImage] = useState("");
  const [idSka, setIdSka] = useState("");

  useEffect(() => {
    getSka(ska, selectSka);
  }, []);

  const getSka = (data, filtered) => {
    const setSka = data.filter((value) => {
      return value.namaSertifikat === filtered;
    });
    setListKeahlian(setSka);
    if (setSka.length > 0) {
      const setData = {
        ska: filtered,
      };
      const formData = new FormData();
      for (const data in setData) {
        formData.append(data, setData[data]);
      }
      props.updateIdentitas(id, formData);
    }
  };

  const handleImage = (event) => {
    setForm({
      fileSertifikat: event.target.files[0],
    });
  };

  const selectedSka = (event) => {
    getSka(ska, event.target.value);
    setSelectSka(event.target.value);
  };

  const deleteKeahlian = (idKeahlian) => {
    props
      .deleteSka(idKeahlian)
      .then((result) => {
        props.getProfileUser(id).then((result) => {
          getSka(result.value.data.data.ska, selectSka);
        });
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const setsKeahlian = (item) => {
    setIsUpdate(true);
    setImage(item.fileSertifikat);
    setIdSka(item.id);
  };

  const updateKeahlian = () => {
    setIsUpdate(false);
    const setData = {
      ...form,
      namaSertifikat: selectSka,
      userId: id,
    };
    const formData = new FormData();
    for (const data in setData) {
      formData.append(data, setData[data]);
    }
    props
      .updateSka(idSka, formData)
      .then((result) => {
        setForm({ fileSertifikat: "" });
        props.getProfileUser(id).then((result) => {
          getSka(result.value.data.data.ska, selectSka);
        });
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const addKeahlian = () => {
    if (!form.fileSertifikat) {
      alert("Input File Sertifikat !");
    } else {
      const setData = {
        ...form,
        namaSertifikat: selectSka,
        userId: id,
      };
      const formData = new FormData();
      for (const data in setData) {
        formData.append(data, setData[data]);
      }
      props
        .postSka(formData)
        .then((result) => {
          setForm({ fileSertifikat: "" });
          props.getProfileUser(id).then((result) => {
            getSka(result.value.data.data.ska, selectSka);
          });
          alert(result.value.data.msg);
        })
        .catch((error) => {
          alert(error.response.data.msg);
        });
    }
  };

  return (
    <div className="mt-3">
      <Row>
        <Col md={12} className="my-2">
          <Form.Group as={Col}>
            <Form.Label>Sertifikat Keahlian</Form.Label>
            <Form.Control
              as="select"
              onChange={(event) => selectedSka(event)}
              value={selectSka}
            >
              <option value="">-- Pilih Sertifikat Keahlian --</option>
              <option value="MUDA">Muda</option>
              <option value="MADYA">Madya</option>
              <option value="UTAMA">Utama</option>
            </Form.Control>
          </Form.Group>
        </Col>
        <hr className="mt-2" />
        <Col md={12} className="mb-2">
          <Form.Group as={Col}>
            <Form.Label>
              Upload File Sertifikat Keahlian{" "}
              {isUpdate && (
                <a href={`${urlImage}uploads/ska/${image}`} target="__blank">
                  (Lihat File Upload)
                </a>
              )}
            </Form.Label>
            <input
              type="file"
              className="form-control"
              onChange={(event) => handleImage(event)}
            />
          </Form.Group>
        </Col>
        <div className="d-grid gap-2 my-3">
          <Button
            variant="outline-primary"
            onClick={isUpdate ? updateKeahlian : addKeahlian}
          >
            {isUpdate ? "Edit" : "Tambah"} File Sertifikat Keahlian
          </Button>
        </div>
        <hr />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Ska</th>
              <th>File Sertifikat</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            {listKeahlian
              ? listKeahlian.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.namaSertifikat}</td>
                    <td>
                      <a
                        href={`${urlImage}uploads/ska/${item.fileSertifikat}`}
                        target="__blank"
                      >
                        {item.fileSertifikat}
                      </a>
                    </td>
                    <td className="text-center">
                      <span
                        onClick={() => setsKeahlian(item)}
                        className="action"
                      >
                        Update
                      </span>{" "}
                      |{" "}
                      <span
                        onClick={() => deleteKeahlian(item.id)}
                        className="action"
                      >
                        Delete
                      </span>
                    </td>
                  </tr>
                ))
              : null}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = {
  getProfileUser,
  updateIdentitas,
  postSka,
  updateSka,
  deleteSka,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormSka);
