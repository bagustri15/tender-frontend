import React from "react";
import { Navbar, Container, Nav, Button, NavDropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import "./index.css";
import { logout } from "../../stores/actions/auth";

function NavbarComponent(props) {
  const token = localStorage.getItem("token");
  const isAdmin = props.user.dataUser.role === "admin";

  const handleNavigation = (navigation) => {
    props.history.push(`/${navigation}`);
  };

  const handleLogout = () => {
    props.logout();
    localStorage.clear();
    props.history.push("/");
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand
          onClick={() => handleNavigation("")}
          style={{ cursor: "pointer" }}
        >
          PT. Tridacomi Andalan Semesta
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {token ? (
            <>
              <Nav className="me-auto">
                <Nav.Link onClick={() => handleNavigation("beranda")}>
                  Beranda
                </Nav.Link>
                {isAdmin ? (
                  <>
                    <NavDropdown title="Kelola" id="basic-nav-dropdown">
                      <NavDropdown.Item
                        onClick={() => handleNavigation("kelola-tender")}
                      >
                        Tender
                      </NavDropdown.Item>
                    </NavDropdown>
                    {/* <Nav.Link onClick={() => handleNavigation("metode")}>
                      Metode
                    </Nav.Link> */}
                  </>
                ) : (
                  <>
                    <Nav.Link onClick={() => handleNavigation("joined-tender")}>
                      Joined Tender
                    </Nav.Link>
                    <Nav.Link onClick={() => handleNavigation("data-diri")}>
                      Data Diri
                    </Nav.Link>
                  </>
                )}
                <Nav.Link onClick={() => handleNavigation("ganti-pass")}>
                  Ganti Pass
                </Nav.Link>
              </Nav>
              <div className="d-grid">
                <Button variant="outline-primary" onClick={handleLogout}>
                  Keluar
                </Button>
              </div>
            </>
          ) : (
            <>
              <Nav className="me-auto">
                <Nav.Link onClick={() => handleNavigation("beranda")}>
                  Beranda
                </Nav.Link>
                <Nav.Link onClick={() => handleNavigation("kontak")}>
                  Kontak Kami
                </Nav.Link>
              </Nav>
              <div className="d-grid">
                <Button
                  variant="outline-primary"
                  className="btn--register"
                  onClick={() => handleNavigation("daftar")}
                >
                  Daftar
                </Button>
              </div>
              <div className="d-grid">
                <Button
                  variant="primary"
                  onClick={() => handleNavigation("masuk")}
                >
                  Masuk
                </Button>
              </div>
            </>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  user: state.user,
});

const mapDispatchToProps = { logout };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NavbarComponent));
