import React, { useState, useEffect } from "react";
import { Modal, Form, Col, Row, Button, Table } from "react-bootstrap";
import { connect } from "react-redux";
import {
  getTahapTender,
  postTahapTender,
  updateTahapTender,
  deleteTahapTender,
} from "../../stores/actions/tahapTender";
import { updateTender } from "../../stores/actions/tender";

function ModalTender(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const { show, handleClose, isUpdate, idTender, tahapTenders } = props;
  const [form, setForm] = useState({
    nama: "",
    tenderId: "",
    fileTahap: null,
    tahapMulai: "",
    tahapSelesai: "",
  });
  const [tahapUpdate, setTahapUpdate] = useState(false);
  const [idTahapTender, setIdTahapTender] = useState("");
  const [idTahap, setIdTahap] = useState("");

  useEffect(() => {
    if (idTender) {
      props.getTahapTender(idTender);
      setForm({
        ...form,
        tenderId: idTender,
      });
      setIdTahap(tahapTenders);
    }
  }, [idTender]);

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleImage = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.files[0],
    });
  };

  const handleAdd = () => {
    const formData = new FormData();
    for (const data in form) {
      formData.append(data, form[data]);
    }
    props.postTahapTender(formData).then((res) => {
      props.getTahapTender(idTender);
      resetForm();
    });
  };

  const setUpdate = (data) => {
    setForm({
      ...form,
      nama: data.nama,
      fileTahap: data.fileTahap,
      tahapMulai: data.tahapMulai.slice(0, 10),
      tahapSelesai: data.tahapSelesai.slice(0, 10),
    });
    setIdTahapTender(data.id);
    setTahapUpdate(true);
  };

  const handleUpdate = () => {
    const formData = new FormData();
    for (const data in form) {
      formData.append(data, form[data]);
    }
    props.updateTahapTender(idTahapTender, formData).then((res) => {
      props.getTahapTender(idTender);
      resetForm();
    });
    setTahapUpdate(false);
  };

  const handleDelete = (id) => {
    props.deleteTahapTender(id).then((res) => {
      props.getTahapTender(idTender);
    });
  };

  const handleActive = (id) => {
    const setData = {
      tahapTenderId: id,
    };
    props.updateTender(idTender, setData).then((res) => {
      setIdTahap(id);
    });
  };

  const resetForm = () => {
    setForm({
      ...form,
      nama: "",
      fileTahap: null,
      tahapMulai: "",
      tahapSelesai: "",
    });
  };

  return (
    <>
      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header>
          <Modal.Title>Manage Tahap Tender</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col md={12} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Nama</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="nama"
                  value={form.nama}
                  placeholder="Masukan Nama Tahap ..."
                />
              </Form.Group>
            </Col>
            <Col md={12} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>
                  Upload File Tahap{" "}
                  {form.fileTahap && (
                    <a
                      href={`${urlImage}uploads/tender/${form.fileTahap}`}
                      target="__blank"
                    >
                      (Lihat File Upload)
                    </a>
                  )}
                </Form.Label>
                <input
                  type="file"
                  className="form-control"
                  name="fileTahap"
                  onChange={(event) => handleImage(event)}
                />
              </Form.Group>
            </Col>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Tanggal Mulai</Form.Label>
                <Form.Control
                  type="date"
                  onChange={(event) => changeText(event)}
                  name="tahapMulai"
                  value={form.tahapMulai}
                />
              </Form.Group>
            </Col>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Tanggal Selesai</Form.Label>
                <Form.Control
                  type="date"
                  onChange={(event) => changeText(event)}
                  name="tahapSelesai"
                  value={form.tahapSelesai}
                />
              </Form.Group>
            </Col>
          </Row>
          <div className="d-grid gap-2 my-3">
            <Button
              variant="outline-primary"
              onClick={tahapUpdate ? handleUpdate : handleAdd}
            >
              {tahapUpdate ? "Edit " : "Tambah "}
              Tahap Tender
            </Button>
          </div>
          <hr />
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Tahap</th>
                <th>File Tahap</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th className="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              {props.tahapTender.data.length > 0 ? (
                props.tahapTender.data.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.nama}</td>
                    <td>{item.fileTahap}</td>
                    <td>{item.tahapMulai}</td>
                    <td>{item.tahapSelesai}</td>
                    <td>
                      <Button
                        variant="primary"
                        size="sm"
                        className="mx-1"
                        onClick={() => handleActive(item.id)}
                        disabled={idTahap === item.id ? true : false}
                      >
                        Aktifkan
                      </Button>
                      <Button
                        variant="secondary"
                        size="sm"
                        className="mx-1"
                        onClick={() => setUpdate(item)}
                      >
                        Update
                      </Button>
                      <Button
                        variant="danger"
                        size="sm"
                        className="mx-1"
                        onClick={() => handleDelete(item.id)}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan="6" className="text-center">
                    -- Data Tidak Ada --
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          {isUpdate ? (
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          ) : (
            <Button variant="primary" onClick={handleClose}>
              Finish
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
}

const mapStateToProps = (state) => ({
  tahapTender: state.tahapTender,
});

const mapDispatchToProps = {
  getTahapTender,
  postTahapTender,
  updateTahapTender,
  deleteTahapTender,
  updateTender,
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalTender);
