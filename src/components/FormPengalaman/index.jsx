import React, { useState } from "react";
import "./index.css";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import { connect } from "react-redux";
import {
  getProfileUser,
  postPengalaman,
  updatePengalaman,
  deletePengalaman,
} from "../../stores/actions/user";

function FormPengalaman(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const { id, pengalaman } = props.user.dataUser;
  const [form, setForm] = useState({
    nama: "",
    deskripsi: "",
    tanggalMulai: "",
    tanggalSelesai: "",
    fileBuktiKerja: "",
  });
  const [idPengalaman, setIdPengalaman] = useState(0);
  const [isUpdate, setIsUpdate] = useState(false);
  const [image, setImage] = useState("");

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleImage = (event) => {
    setForm({
      ...form,
      fileBuktiKerja: event.target.files[0],
    });
  };

  const addPengalaman = () => {
    if (!form.fileBuktiKerja) {
      alert("Input File Bukti Pernah Bekerja !");
    } else {
      const setData = {
        ...form,
        userId: id,
      };
      console.log(setData);
      const formData = new FormData();
      for (const data in setData) {
        formData.append(data, setData[data]);
      }
      props
        .postPengalaman(formData)
        .then((result) => {
          props.getProfileUser(id);
          setForm({
            nama: "",
            deskripsi: "",
            tanggalMulai: "",
            tanggalSelesai: "",
            fileBuktiKerja: "",
          });
          alert(result.value.data.msg);
        })
        .catch((error) => {
          alert(error.response.data.msg);
        });
    }
  };

  const setPengalaman = (data) => {
    const {
      id,
      nama,
      deskripsi,
      tanggalMulai,
      tanggalSelesai,
      fileBuktiKerja,
    } = data;
    setIsUpdate(true);
    setIdPengalaman(id);
    setImage(fileBuktiKerja);
    setForm({
      nama,
      deskripsi,
      tanggalMulai: tanggalMulai.slice(0, 10),
      tanggalSelesai: tanggalSelesai.slice(0, 10),
    });
  };

  const updatePengalaman = () => {
    setIsUpdate(false);
    const setData = {
      ...form,
      userId: id,
    };
    const formData = new FormData();
    for (const data in setData) {
      formData.append(data, setData[data]);
    }
    props
      .updatePengalaman(idPengalaman, formData)
      .then((result) => {
        props.getProfileUser(id);
        setForm({
          nama: "",
          deskripsi: "",
          tanggalMulai: "",
          tanggalSelesai: "",
          fileBuktiKerja: "",
        });
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  const deletePengalaman = (idPengalaman) => {
    props
      .deletePengalaman(idPengalaman)
      .then((result) => {
        props.getProfileUser(id);
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };

  return (
    <div className="mt-3">
      <Row>
        <Col md={4}>
          <Form.Group as={Col}>
            <Form.Label>Pengalaman</Form.Label>
            <Form.Control
              placeholder="Masukkan Pengalaman"
              name="nama"
              value={form.nama}
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <Col md={4}>
          <Form.Group as={Col}>
            <Form.Label>Deskripsi</Form.Label>
            <Form.Control
              placeholder="Masukkan Deskripsi"
              name="deskripsi"
              value={form.deskripsi}
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <Col md={4}>
          <Form.Group as={Col}>
            <Form.Label>
              Bukti Pernah Bekerja{" "}
              {isUpdate && (
                <a
                  href={`${urlImage}uploads/pengalaman/${image}`}
                  target="__blank"
                >
                  (Lihat File Upload)
                </a>
              )}
            </Form.Label>
            <input
              type="file"
              className="form-control"
              onChange={(event) => handleImage(event)}
            />
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group as={Col}>
            <Form.Label>Tanggal Mulai</Form.Label>
            <Form.Control
              type="date"
              name="tanggalMulai"
              value={form.tanggalMulai}
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group as={Col}>
            <Form.Label>Tanggal Selesai</Form.Label>
            <Form.Control
              type="date"
              name="tanggalSelesai"
              value={form.tanggalSelesai}
              onChange={(event) => changeText(event)}
            />
          </Form.Group>
        </Col>
      </Row>
      <div className="d-grid gap-2 my-3">
        <Button
          variant="outline-primary"
          onClick={isUpdate ? updatePengalaman : addPengalaman}
        >
          {isUpdate ? "Edit" : "Tambah"} Pengalaman
        </Button>
      </div>
      <hr />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          {pengalaman
            ? pengalaman.map((item, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.nama}</td>
                  <td>{item.deskripsi}</td>
                  <td>{item.tanggalMulai}</td>
                  <td>{item.tanggalSelesai}</td>
                  <td className="text-center">
                    <span
                      onClick={() => setPengalaman(item)}
                      className="action"
                    >
                      Update
                    </span>{" "}
                    |{" "}
                    <span
                      onClick={() => deletePengalaman(item.id)}
                      className="action"
                    >
                      Delete
                    </span>
                  </td>
                </tr>
              ))
            : null}
        </tbody>
      </Table>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});
const mapDispatchToProps = {
  getProfileUser,
  postPengalaman,
  updatePengalaman,
  deletePengalaman,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormPengalaman);
