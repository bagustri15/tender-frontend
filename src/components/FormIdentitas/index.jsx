import React, { useState } from "react";
import "./index.css";
import { Row, Col, Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { getProfileUser, updateIdentitas } from "../../stores/actions/user";
import dataProvinsi from "./provinsi";

function FormIdentitas(props) {
  const urlImage = process.env.REACT_APP_BASE_URL;
  const {
    id,
    nama,
    tempatLahir,
    tanggalLahir,
    pendidikanTerakhir,
    jurusan,
    fileIjazahTerakhir,
    gaji,
    alamat,
    provinsi,
    kabupaten,
    kodePos,
    email,
    noTelp,
    nik,
    scanKtp,
  } = props.user.dataUser;
  const [form, setForm] = useState({
    nama: nama || "",
    tempatLahir: tempatLahir || "",
    tanggalLahir: tanggalLahir || "",
    pendidikanTerakhir: pendidikanTerakhir || "",
    jurusan: jurusan || "",
    fileIjazahTerakhir: fileIjazahTerakhir || "",
    gaji: gaji || "",
    alamat: alamat || "",
    provinsi: provinsi || "",
    kabupaten: kabupaten || "",
    kodePos: kodePos || "",
    email,
    noTelp: noTelp || "",
    nik: nik || "",
    scanKtp: scanKtp || "",
  });

  const changeText = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleImage = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.files[0],
    });
  };

  const handleSubmit = () => {
    const formData = new FormData();
    for (const data in form) {
      formData.append(data, form[data]);
    }
    props
      .updateIdentitas(id, formData)
      .then((result) => {
        props.getProfileUser(id);
        alert(result.value.data.msg);
      })
      .catch((error) => {
        alert(error.response.data.msg);
      });
  };
  console.log(dataProvinsi);
  return (
    <Row className="mt-3">
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Nama Lengkap</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="nama"
            value={form.nama}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Tempat Lahir</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="tempatLahir"
            placeholder="Masukkan Tempat Lahir"
            value={form.tempatLahir}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Tanggal Lahir</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            type="date"
            name="tanggalLahir"
            value={form.tanggalLahir}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Pendidikan Terakhir</Form.Label>
          <Form.Control
            as="select"
            onChange={(event) => changeText(event)}
            name="pendidikanTerakhir"
            value={form.pendidikanTerakhir}
          >
            <option value="">-- Pilih Pendidikan Terakhir --</option>
            <option value="D3">D3</option>
            <option value="S1">S1</option>
            <option value="S2">S2</option>
          </Form.Control>
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>jurusan</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="jurusan"
            placeholder="Masukkan jurusan"
            value={form.jurusan}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>
            Upload Ijazah Pendidikan Terakhir{" "}
            {fileIjazahTerakhir && (
              <a
                href={`${urlImage}uploads/user/${fileIjazahTerakhir}`}
                target="__blank"
              >
                (Lihat File Upload)
              </a>
            )}
          </Form.Label>
          <input
            type="file"
            className="form-control"
            name="fileIjazahTerakhir"
            onChange={(event) => handleImage(event)}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Gaji Perbulan</Form.Label>
          <Form.Control
            type="number"
            onChange={(event) => changeText(event)}
            name="gaji"
            placeholder="Masukkan Gaji"
            value={form.gaji}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Alamat</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="alamat"
            placeholder="Masukkan Alamat"
            value={form.alamat}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Provinsi</Form.Label>
          <Form.Control
            as="select"
            onChange={(event) => changeText(event)}
            name="provinsi"
            value={form.provinsi}
          >
            <option value="">-- Pilih Provinsi --</option>
            {dataProvinsi.map((item) => (
              <option value={item} key={item}>
                {item}
              </option>
            ))}
          </Form.Control>
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Kabupaten/Kota</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="kabupaten"
            placeholder="Masukkan Kabupaten"
            value={form.kabupaten}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Kode Pos</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="kodePos"
            placeholder="Masukkan Kode Pos"
            value={form.kodePos}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>Email</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="email"
            value={form.email}
            disabled
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>No. Telepon</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="noTelp"
            placeholder="Masukkan No Telp"
            value={form.noTelp}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>NIK</Form.Label>
          <Form.Control
            onChange={(event) => changeText(event)}
            name="nik"
            placeholder="Masukkan No Induk KTP"
            value={form.nik}
          />
        </Form.Group>
      </Col>
      <Col md={4} className="my-2">
        <Form.Group as={Col}>
          <Form.Label>
            Upload Scan KTP{" "}
            {scanKtp && (
              <a href={`${urlImage}uploads/user/${scanKtp}`} target="__blank">
                (Lihat File Upload)
              </a>
            )}
          </Form.Label>
          <input
            type="file"
            className="form-control"
            name="scanKtp"
            onChange={(event) => handleImage(event)}
          />
        </Form.Group>
      </Col>
      <div className="d-grid gap-2 my-3">
        <Button variant="primary" onClick={handleSubmit}>
          Simpan
        </Button>
      </div>
    </Row>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = { getProfileUser, updateIdentitas };

export default connect(mapStateToProps, mapDispatchToProps)(FormIdentitas);
