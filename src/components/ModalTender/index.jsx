import React, { useState, useEffect } from "react";
import {
  Modal,
  Form,
  Col,
  Row,
  Button,
  Accordion,
  Card,
} from "react-bootstrap";
import dataProvinsi from "../FormIdentitas/provinsi";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

function ModalTender(props) {
  const {
    show,
    handleClose,
    isUpdate,
    setData,
    handleAddTender,
    handleUpdateTender,
    handleShowModalTahap,
  } = props;
  const [form, setForm] = useState({
    nama: "",
    jabatan: [],
    instansi: "",
    satuanKerja: "",
    kategori: "",
    nilaiPaguPaket: "",
    provinsi: "",
    deskripsi: "",
    isActive: "",
  });
  const [jabatan, setJabatan] = useState("");
  useEffect(() => {
    setUpdate();
  }, [setData]);

  const setUpdate = () => {
    const {
      nama,
      jabatan,
      instansi,
      satuanKerja,
      nilaiPaguPaket,
      provinsi,
      kategori,
      deskripsi,
      isActive,
    } = setData;
    setForm({
      nama: nama ? nama : "",
      jabatan: jabatan ? jabatan.split(",") : [],
      instansi: instansi ? instansi : "",
      satuanKerja: satuanKerja ? satuanKerja : "",
      kategori: kategori ? kategori : "",
      nilaiPaguPaket: nilaiPaguPaket ? nilaiPaguPaket : "",
      provinsi: provinsi ? provinsi : "",
      deskripsi: deskripsi ? deskripsi : "",
      isActive: isActive ? isActive : "",
    });
  };

  const changeText = (event) => {
    const { name, value } = event.target;
    setForm({
      ...form,
      [name]: value,
    });
  };
  const changeJabatan = (event) => {
    const { name, value } = event.target;
    if (event.key === "Enter") {
      setForm({
        ...form,
        [name]: [...form.jabatan, value],
      });
      setJabatan("");
    }
  };
  const handleDeleteJabatan = (data) => {
    const deleteData = form.jabatan.filter((item) => item !== data);
    setForm({ ...form, jabatan: deleteData });
  };

  const handleAdd = () => {
    const setData = {
      ...form,
      jabatan: form.jabatan.join(),
    };
    handleAddTender(setData);
  };
  const handleUpdate = () => {
    const setData = {
      ...form,
      jabatan: form.jabatan.join(),
    };
    handleUpdateTender(setData);
  };

  console.log(form.deskripsi);

  return (
    <>
      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header>
          <Modal.Title>{isUpdate ? "Update" : "Tambah"} Tender</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Nama</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="nama"
                  value={form.nama}
                  placeholder="Masukan Nama Tender ..."
                />
              </Form.Group>
            </Col>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Status Tender</Form.Label>
                <Form.Control
                  as="select"
                  onChange={(event) => changeText(event)}
                  name="isActive"
                  value={form.isActive}
                >
                  <option value={null}>-- Pilih Status Tender --</option>
                  <option value="true">Aktif</option>
                  <option value="false">Tidak Aktif</option>
                </Form.Control>
              </Form.Group>
            </Col>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Instansi</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="instansi"
                  value={form.instansi}
                  placeholder="Masukan Instansi Tender ..."
                />
              </Form.Group>
            </Col>
            <Col md={6} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Satuan Kerja</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="satuanKerja"
                  value={form.satuanKerja}
                  placeholder="Masukan Satuan Kerja Tender ..."
                />
              </Form.Group>
            </Col>
            <Col md={4} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Kategori</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="kategori"
                  value={form.kategori}
                  placeholder="Masukan Kategori Tender ..."
                />
              </Form.Group>
            </Col>
            <Col md={4} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Nilai Pagu Paket</Form.Label>
                <Form.Control
                  onChange={(event) => changeText(event)}
                  name="nilaiPaguPaket"
                  value={form.nilaiPaguPaket}
                  placeholder="Masukan Nilai Pagu Paket Tender ..."
                />
              </Form.Group>
            </Col>
            <Col md={4} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Lokasi</Form.Label>
                <Form.Control
                  as="select"
                  onChange={(event) => changeText(event)}
                  name="provinsi"
                  value={form.provinsi}
                >
                  <option value="">-- Pilih Provinsi --</option>
                  {dataProvinsi.map((item) => (
                    <option value={item} key={item}>
                      {item}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Col>
            <Col md={12} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Jabatan</Form.Label>
                <Form.Control
                  onKeyPress={(event) => changeJabatan(event)}
                  onChange={(event) => setJabatan(event.target.value)}
                  name="jabatan"
                  value={jabatan}
                  placeholder="Masukan Jabatan Lalu Enter ..."
                />
              </Form.Group>
              <div className="my-3">
                {form.jabatan.map((item) => (
                  <Button
                    variant="primary"
                    key={item}
                    className="mx-2"
                    onClick={() => handleDeleteJabatan(item)}
                  >
                    {item}
                  </Button>
                ))}
              </div>
            </Col>
            {isUpdate && (
              <Col md={12} className="my-2">
                <Accordion>
                  <Card>
                    <Accordion.Toggle as={Card.Header} eventKey="0">
                      Deskripsi Sebelumnya
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: setData.deskripsi,
                          }}
                        />
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              </Col>
            )}
            <Col md={12} className="my-2">
              <Form.Group as={Col}>
                <Form.Label>Deskripsi</Form.Label>
                <CKEditor
                  editor={ClassicEditor}
                  data={form.deskripsi}
                  onChange={(event, editor) => {
                    const data = editor.getData();
                    setForm({
                      ...form,
                      deskripsi: data,
                    });
                  }}
                />
              </Form.Group>
            </Col>
            {isUpdate && (
              <Col md={12} className="my-2">
                <div className="d-grid">
                  <Button variant="primary" onClick={handleShowModalTahap}>
                    Manage Tahap Tender
                  </Button>
                </div>
              </Col>
            )}
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button
            variant="primary"
            onClick={isUpdate ? handleUpdate : handleAdd}
          >
            {isUpdate ? "Update" : "Simpan"}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalTender;
