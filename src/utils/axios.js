import axios from "axios";
// console.log(process.env.REACT_APP_BASE_URL);
const axiosApiIntances = axios.create({
  baseURL: `${process.env.REACT_APP_BASE_URL}`,
});

// Add a response interceptor
axiosApiIntances.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.response.status === 403) {
      localStorage.clear();
            window.location.href = "/masuk";
    }
    return Promise.reject(error);
  }
);

export default axiosApiIntances;
