import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/scss/custom.scss";
import "./assets/css/styles.css";
import "react-progress-2/main.css";

import { Provider } from "react-redux";
import storage from "./stores/store";

import { PersistGate } from "redux-persist/integration/react";
const { store, persistor } = storage;

ReactDOM.render(
  <React.Fragment>
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.Fragment>,
  document.getElementById("root")
);
