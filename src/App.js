import { BrowserRouter as Router, Switch } from "react-router-dom";

import Progress from "react-progress-2";

// AUTH
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";

// USER
import Home from "./pages/main/Home";
import Beranda from "./pages/main/Beranda";
import JoinedTender from "./pages/main/JoinedTender";
import DetailTender from "./pages/main/DetailTender";
import DataDiri from "./pages/main/DataDiri";
import GantiPass from "./pages/main/GantiPass";
import Kontak from "./pages/main/Kontak";

// ADMIN
import Seleksi from "./pages/main/Seleksi";
import KelolaTender from "./pages/main/KelolaTender";
import Metode from "./pages/main/Metode/SAW";

// COMPONENT
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

// ROUTE
import PrivateRoute from "./helpers/route/private";
import PublicRoute from "./helpers/route/public";

function App(props) {
  return (
    <Router>
      <Progress.Component />
      <Navbar {...props} />
      <Switch>
        <PublicRoute path="/masuk" exact component={Login} restricted={true} />
        <PublicRoute
          path="/daftar"
          exact
          component={Register}
          restricted={true}
        />
        <PublicRoute path="/" exact component={Home} />
        <PublicRoute path="/beranda" exact component={Beranda} />
        <PublicRoute path="/kontak" exact component={Kontak} />
        <PrivateRoute path="/joined-tender" exact component={JoinedTender} />
        <PrivateRoute path="/detail-tender" exact component={DetailTender} />
        <PrivateRoute path="/data-diri" exact component={DataDiri} />
        <PrivateRoute path="/ganti-pass" exact component={GantiPass} />
        <PrivateRoute path="/metode" exact component={Metode} />
        <PrivateRoute path="/kelola-tender" exact component={KelolaTender} />
        <PrivateRoute path="/seleksi/:id" exact component={Seleksi} />
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
