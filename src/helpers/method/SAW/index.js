import bobot from "./bobot.js"
import attribut from "./attribut.js"
import {
  riwayatPendidikan,
  jumlahPengalamanKerja,
  jumlahTahunKerja,
  usia,
  gaji,
  ska,
  alamat,
} from "./kriteria.js"

function run(data) {
  const dataBaru = data.map((item) => {
    return {
      ...item,
      gaji: item.gaji / 1000000,
    };
  });
  const dataAnalysis = analysis(dataBaru);
  const dataNormal = getNormal(dataAnalysis);
  const dataBobotNormal = getBobotNormal();
  const dataTerbobot = getTerbobot(dataNormal, dataBobotNormal);
  const dataTotal = getTotal(dataTerbobot);
  const dataRank = getRank(dataTotal);
  return {
    dataAnalysis,
    dataNormal,
    dataBobotNormal,
    dataTerbobot,
    dataTotal,
    dataRank,
  };
}

function analysis(data) {
  const newData = data.map((item) => {
    const C1 = riwayatPendidikan.find(
      (el) => el.riwayatPendidikan === item.riwayatPendidikan
    ).bobot;
    const C2 = jumlahPengalamanKerja.find((el) =>
      eval(`${item.jumlahPengalamanKerja} ${el.jumlahPengalamanKerja}`)
    ).bobot;
    const C3 = jumlahTahunKerja.find((el) =>
      eval(`${item.jumlahTahunKerja} ${el.jumlahTahunKerja}`)
    ).bobot;
    const C4 = ska.find((el) => el.ska === item.ska).bobot;
    const C5 = usia.find((el) => eval(`${item.usia} ${el.usia}`)).bobot;
    const C6 = alamat.find((el) => el.alamat === item.alamat).bobot;
    const C7 = gaji.find((el) => eval(`${item.gaji} ${el.gaji}`)).bobot;
    return [C1, C2, C3, C4, C5, C6, C7];
  });
  return newData;
}

function getNormal(data) {
  // membalik matrik nilai kolom dan baris
  const arr = [[], [], [], [], [], [], []];
  data.forEach((val, key) => {
    val.forEach((v, k) => {
      arr[k].push(v);
    });
  });

  let min = [];
  let max = [];
  arr.forEach((val, key) => {
    min[key] = Math.min(...val);
    max[key] = Math.max(...val);
  });

  const normal = [];
  data.forEach((val, key) => {
    let data = [];
    val.forEach((v, k) => {
      if (k <= attribut.length) {
        const nilaiBobot = attribut[k] === "benefit" ? v / max[k] : min[k] / v;
        data = [...data, nilaiBobot];
      }
    });
    normal.push(data);
  });

  return normal;
}

function getBobotNormal() {
  let bobotNormal = [];
  const totalBobot = bobot.reduce((a, b) => a + b, 0);
  bobot.forEach((val, key) => {
    bobotNormal[key] = val / totalBobot;
  });
  return bobotNormal;
}

function getTerbobot(dataNormal, dataBobotNormal) {
  const terbobot = [];
  dataNormal.forEach((val, key) => {
    let data = [];
    val.forEach((v, k) => {
      if (k <= attribut.length) {
        const nilaiTerbobot = v * dataBobotNormal[k];
        data = [...data, nilaiTerbobot];
      }
    });
    terbobot.push(data);
  });
  return terbobot;
}

function getTotal(dataTerbobot) {
  let total = [];
  dataTerbobot.forEach((val, key) => {
    total[key] = val.reduce((a, b) => a + b, 0);
  });
  return total;
}

function getRank(total) {
  var sorted = total.slice().sort(function (a, b) {
    return b - a;
  });
  var ranks = total.map(function (v) {
    return sorted.indexOf(v) + 1;
  });
  return ranks;
}

export default run