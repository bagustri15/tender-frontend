export const riwayatPendidikan = [
  { riwayatPendidikan: "D3", bobot: 0.6 },
  { riwayatPendidikan: "S1", bobot: 0.8 },
  { riwayatPendidikan: "S2", bobot: 1 },
];

export const jumlahPengalamanKerja = [
  { jumlahPengalamanKerja: "< 1", bobot: 0.4 },
  { jumlahPengalamanKerja: "<= 2", bobot: 0.6 },
  { jumlahPengalamanKerja: "<= 4", bobot: 0.8 },
  { jumlahPengalamanKerja: ">= 5", bobot: 1 },
];

export const jumlahTahunKerja = [
  { jumlahTahunKerja: "< 1", bobot: 0.4 },
  { jumlahTahunKerja: "<= 2", bobot: 0.6 },
  { jumlahTahunKerja: "<= 4", bobot: 0.8 },
  { jumlahTahunKerja: ">= 5", bobot: 1 },
];
export const usia = [
  { usia: "<= 23", bobot: 0.2 },
  { usia: "<= 27", bobot: 0.6 },
  { usia: "<= 35", bobot: 0.8 },
  { usia: "<= 55", bobot: 1 },
  { usia: ">= 56", bobot: 0.4 },
];
export const gaji = [
  { gaji: "<= 7", bobot: 1 },
  { gaji: "<= 10", bobot: 0.8 },
  { gaji: "<= 13", bobot: 0.6 },
  { gaji: "<= 16", bobot: 0.4 },
  { gaji: ">= 17", bobot: 0.2 },
];
export const ska = [
  { ska: "MUDA", bobot: 0.2 },
  { ska: "MADYA", bobot: 0.8 },
  { ska: "UTAMA", bobot: 1 },
];
export const alamat = [
  { alamat: "JAUH", bobot: 0.4 },
  { alamat: "SEDANG", bobot: 0.8 },
  { alamat: "DEKAT", bobot: 1 },
];

export const allKriteria = [
  {
    kode: "C1",
    nama: "Riwayat Pendidikan",
    attribut: "benefit",
    crips: "D3 = 0.6 | S1 = 0.8 | S2 = 1",
    bobot: "10",
  },
  {
    kode: "C2",
    nama: "Jumlah Pengalaman Kerja",
    attribut: "benefit",
    crips: "0 = 0.4 | 1-2 = 0.6 | 3-4 = 0.8 | >=5 = 1",
    bobot: "15",
  },
  {
    kode: "C3",
    nama: "Jumlah Tahun Kerja",
    attribut: "benefit",
    crips: "0 = 0.4 | 1-2 = 0.6 | 3-4 = 0.8 | >=5 = 1",
    bobot: "20",
  },
  {
    kode: "C4",
    nama: "Sertifikat Keahlian",
    attribut: "benefit",
    crips: "MUDA = 0.2 | MADYA = 0.8 | UTAMA = 1",
    bobot: "25",
  },
  {
    kode: "C5",
    nama: "Usia",
    attribut: "benefit",
    crips:
      "0 - 23 = 0.2 | 24 - 27 = 0.6 | 28 - 35 = 0.8 | 36 - 55 = 1 | >= 56 = 0.4",
    bobot: "5",
  },
  {
    kode: "C6",
    nama: "Alamat",
    attribut: "cost",
    crips: "JAUH = 0.4 | SEDANG = 0.8 | DEKAT = 1",
    bobot: "10",
  },
  {
    kode: "C7",
    nama: "Gaji Perbulan",
    attribut: "cost",
    crips:
      "0 - 7 = 1 | 8 - 10 = 0.8 | 11 - 13 = 0.6 | 14 - 16 = 0.4 | >=17 = 0.2",
    bobot: "15",
  },
];

// module.exports = {
//   riwayatPendidikan,
//   jumlahPengalamanKerja,
//   jumlahTahunKerja,
//   usia,
//   gaji,
//   ska,
//   alamat,
//   allKriteria,
// };
