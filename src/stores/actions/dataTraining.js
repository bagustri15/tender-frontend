import axiosApiIntances from "../../utils/axios";

export const getAllDataTraining = () => {
  return {
    type: "GET_ALL_DATA_TRAINING",
    payload: axiosApiIntances.get(`data-training`),
  };
};

export const getDataTrainingByCondition = (data) => {
  return {
    type: "GET_DATA_TRAINING_CONDITION",
    payload: axiosApiIntances.post(`data-training/condition`, data),
  };
};

export const getDataResult = () => {
  return {
    type: "GET_DATA_RESULT",
    payload: axiosApiIntances.get(`data-training/result`),
  };
};
