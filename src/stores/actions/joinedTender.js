import axiosApiIntances from "../../utils/axios";

export const getAllDataJoined = (id) => {
  return {
    type: "GET_ALL_DATA_JOINED",
    payload: axiosApiIntances.get(`join-tender/user-id/${id}`),
  };
};

export const getAllDataJoinedTender = (id, jabatan) => {
  return {
    type: "GET_ALL_JOINED_TENDER",
    payload: axiosApiIntances.get(`join-tender/tender-id/${id}/${jabatan}`),
  };
};

export const postJoinedTender = (data) => {
  return {
    type: "POST_JOINED_TENDER",
    payload: axiosApiIntances.post("join-tender", data),
  };
};

export const patchJoinedTender = (id, data) => {
  return {
    type: "PATCH_JOINED_TENDER",
    payload: axiosApiIntances.patch(`join-tender/${id}`, data),
  };
};
