import axiosApiIntances from "../../utils/axios";

export const getTahapTender = (id) => {
  return {
    type: "GET_TAHAP_TENDER",
    payload: axiosApiIntances.get(`tahap-tender/${id}`),
  };
};

export const postTahapTender = (data) => {
  return {
    type: "POST_TAHAP_TENDER",
    payload: axiosApiIntances.post(`tahap-tender`, data),
  };
};

export const updateTahapTender = (id, data) => {
  return {
    type: "UPDATE_TAHAP_TENDER",
    payload: axiosApiIntances.patch(`tahap-tender/${id}`, data),
  };
};

export const deleteTahapTender = (id) => {
  return {
    type: "DELETE_TAHAP_TENDER",
    payload: axiosApiIntances.delete(`tahap-tender/${id}`),
  };
};
