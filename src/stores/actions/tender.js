import axiosApiIntances from "../../utils/axios";

export const getAllDataTender = (page, limit, search, isActive) => {
  return {
    type: "GET_ALL_DATA_TENDER",
    payload: axiosApiIntances.get(
      `tender?page=${page}&limit=${limit}&search=${search}&isActive=${isActive}`
    ),
  };
};

export const getDataTenderById = (id) => {
  return {
    type: "GET_TENDER_BY_ID",
    payload: axiosApiIntances.get(`tender/${id}`),
  };
};

export const sendMail = (id, status) => {
  return {
    type: "SEND_MAIL",
    payload: axiosApiIntances.get(
      `tender/sendmail/user?id=${id}&status=${status}`
    ),
  };
};

export const exportTender = (id) => {
  return {
    type: "EXPORT_TENDER",
    payload: axiosApiIntances.get(`export/${id}`),
  };
};

export const postTender = (data) => {
  return {
    type: "POST_TENDER",
    payload: axiosApiIntances.post(`tender`, data),
  };
};

export const updateTender = (id, data) => {
  return {
    type: "UPDATE_TENDER",
    payload: axiosApiIntances.patch(`tender/${id}`, data),
  };
};

export const deleteTender = (id) => {
  return {
    type: "DELETE_TENDER",
    payload: axiosApiIntances.delete(`tender/${id}`),
  };
};
