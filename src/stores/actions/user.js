import axiosApiIntances from "../../utils/axios";

export const getProfileUser = (id) => {
  return {
    type: "GET_PROFILE_USER",
    payload: axiosApiIntances.get(`user/profile/${id}`),
  };
};

export const getDetailUser = (id) => {
  return {
    type: "GET_DETAIL_USER",
    payload: axiosApiIntances.get(`user/profile/${id}`),
  };
};

export const updatePassword = (id, data) => {
  return {
    type: "UPDATE_PASSWORD",
    payload: axiosApiIntances.patch(`user/password/${id}`, data),
  };
};

export const updateIdentitas = (id, data) => {
  return {
    type: "UPDATE_IDENTITAS",
    payload: axiosApiIntances.patch(`user/profile/${id}`, data),
  };
};

export const updatePajak = (id, data) => {
  return {
    type: "UPDATE_PAJAK",
    payload: axiosApiIntances.patch(`user/pajak/${id}`, data),
  };
};

export const postPengalaman = (data) => {
  return {
    type: "POST_PENGALAMAN",
    payload: axiosApiIntances.post(`user/pengalaman`, data),
  };
};

export const updatePengalaman = (id, data) => {
  return {
    type: "UPDATE_PENGALAMAN",
    payload: axiosApiIntances.patch(`user/pengalaman/${id}`, data),
  };
};

export const deletePengalaman = (id) => {
  return {
    type: "DELETE_PENGALAMAN",
    payload: axiosApiIntances.delete(`user/pengalaman/${id}`),
  };
};

export const postSka = (data) => {
  return {
    type: "POST_SKA",
    payload: axiosApiIntances.post(`user/ska`, data),
  };
};

export const updateSka = (id, data) => {
  return {
    type: "UPDATE_SKA",
    payload: axiosApiIntances.patch(`user/ska/${id}`, data),
  };
};

export const deleteSka = (id) => {
  return {
    type: "DELETE_SKA",
    payload: axiosApiIntances.delete(`user/ska/${id}`),
  };
};
