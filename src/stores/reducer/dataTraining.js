const initialState = {
  data: [],
  dataTraining: [],
  dataResult: {},
  isLoading: false,
  isError: false,
  msg: "",
};

const dataTraining = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALL_DATA_TRAINING_PENDING":
      return {
        ...state,
        isLoading: true,
        isError: false,
        msg: "",
      };
    case "GET_ALL_DATA_TRAINING_FULFILLED":
      return {
        ...state,
        isLoading: false,
        isError: false,
        dataTraining: action.payload.data.data,
        msg: action.payload.data.msg,
      };
    case "GET_ALL_DATA_TRAINING_REJECTED":
      return {
        ...state,
        isLoading: false,
        isError: true,
        dataTraining: [],
        msg: action.payload.response.data.msg,
      };
    case "GET_DATA_TRAINING_CONDITION_PENDING":
      return {
        ...state,
        isLoading: true,
        isError: false,
        msg: "",
      };
    case "GET_DATA_TRAINING_CONDITION_FULFILLED":
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload.data.data,
        msg: action.payload.data.msg,
      };
    case "GET_DATA_TRAINING_CONDITION_REJECTED":
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: [],
        msg: action.payload.response.data.msg,
      };
    case "GET_DATA_RESULT_PENDING":
      return {
        ...state,
        isLoading: true,
        isError: false,
        msg: "",
      };
    case "GET_DATA_RESULT_FULFILLED":
      return {
        ...state,
        isLoading: false,
        isError: false,
        dataResult: action.payload.data.data,
        msg: action.payload.data.msg,
      };
    case "GET_DATA_RESULT_REJECTED":
      return {
        ...state,
        isLoading: false,
        isError: true,
        dataResult: {},
        msg: action.payload.response.data.msg,
      };
    default:
      return state;
  }
};

export default dataTraining;
