import { combineReducers } from "redux";

import dataTraining from "./dataTraining";
import auth from "./auth";
import user from "./user";
import tender from "./tender";
import joinedTender from "./joinedTender";
import tahapTender from "./tahapTender";

export default combineReducers({
  dataTraining,
  auth,
  user,
  tender,
  joinedTender,
  tahapTender,
});
